package com.mgil.java.ee.fundamentals.servlets.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.util.Date;


@XmlRootElement
public class Product {

    @XmlElement(name="description")
    private String description;
    @XmlElement(name="price")
    private BigDecimal price;
    @XmlElement(name="updated")
    private Date updatedAt;
    @XmlAttribute(name="category")
    private String category;

    public Product() {
    }

    public Product(String description, BigDecimal price, Date updatedAt, String category) {
        this.description = description;
        this.price = price;
        this.updatedAt = updatedAt;
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
