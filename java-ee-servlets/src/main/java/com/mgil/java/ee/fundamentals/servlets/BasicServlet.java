package com.mgil.java.ee.fundamentals.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import static java.lang.System.*;


//Annotation for define the name and the URL of the servlet, this annotation works only if the class extends from a Servlet
@WebServlet(name = "BasicServlet", urlPatterns = "/servlets/basic")
public class BasicServlet extends HttpServlet {


    //TODO: Search a logger for this

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        out.println("Inside doGet()");

        resp.setContentType("text/html");

        StringBuilder stringBuilder = new StringBuilder("<html><head><title>Basic Servlet</title></head><body><h1>Loaded from a servlet</h1></body></html>");

        PrintWriter printWriter = resp.getWriter();

        printWriter.print(stringBuilder.toString());



        /*
            req.getParameter("MyKey");  //Returns the value of the parameter name. Returns null if the parameter does not exist
            req.getParameterValues("MyKey"); //Array of String values of the parameter name if exists
            req.getParameterNames(); // Array with the parameter key
            req.getParameterMap(); //Map of key-value
         */


        //Reading data from a query string

        String customerName = req.getParameter("firstName");
        String customerLastName = req.getParameter("lastName");
        String customerId = req.getParameter("documentId");

        out.println("GET Request ");
        out.println("=============");


        System.out.println("Parameters name");

        Enumeration<String> names = req.getParameterNames();

        while (names.hasMoreElements()) {

            System.out.printf("->%s\n", names.nextElement());

        }


        System.out.println("Customer name: " + customerName);
        System.out.println("Customer last name: " + customerLastName);
        System.out.println("Customer Document ID: " + customerId);

        out.println("Exiting of doGet()");


    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        System.out.println("Client POST Request");

        Enumeration<String> names  = req.getParameterNames();


        while(names.hasMoreElements()) {


            System.out.println(names.nextElement());


        }

    }
}
