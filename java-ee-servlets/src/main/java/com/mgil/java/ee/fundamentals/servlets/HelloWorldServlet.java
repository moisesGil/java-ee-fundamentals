package com.mgil.java.ee.fundamentals.servlets;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * GenericServlet is independent of protocols
 */
@WebServlet("/hello-world")
public class HelloWorldServlet extends GenericServlet {


    @Override
    public void init() throws ServletException {
        super.init();

        System.out.println("Called the init method of the Servlet");
    }


    @Override
    public void destroy() {

        System.out.println("Called the destroy method on the Servlet");

        super.destroy();

    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {

        servletResponse.setContentType("text/plain");

        PrintWriter servletOutWriter = servletResponse.getWriter();
        servletOutWriter.print("Hello world from servlet");

    }






}
