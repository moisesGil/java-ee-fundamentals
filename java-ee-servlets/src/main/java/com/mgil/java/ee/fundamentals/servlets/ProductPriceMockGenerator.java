package com.mgil.java.ee.fundamentals.servlets;

import com.mgil.java.ee.fundamentals.servlets.model.Product;
import com.mgil.java.ee.fundamentals.servlets.model.Products;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class ProductPriceMockGenerator {

    public Products getProducts(){


        String category = "fuel";
        Random random = new Random();

        String descriptions [] = {"Gasolina Premium","Gasolina Regular","Galosina Optimo","Gasolina Regular","Avtur","Kerosene","GLP","Gas Natural"};
        ArrayList<Product> products = new ArrayList<>();

        for(String description : descriptions){

            Product product = new Product();

            product.setCategory(category);
            product.setDescription(description);
            product.setPrice(BigDecimal.valueOf(random.nextInt(300) + 1));
            product.setUpdatedAt(new Date());


            products.add(product);

        }


        Products ps = new Products();

        ps.setProducts(products);

        return ps;

    }

}
