package com.mgil.java.ee.fundamentals.servlets;

import com.mgil.java.ee.fundamentals.servlets.model.Products;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;

@WebServlet(urlPatterns = "/products")
public class ProductPriceServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //TODO: use EJB for this

        ProductPriceMockGenerator generator = new ProductPriceMockGenerator();

        Products products = generator.getProducts();

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Products.class);

            Marshaller marshaller  = jaxbContext.createMarshaller();

            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,true);

            marshaller.marshal(products,resp.getWriter());

        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }
}
