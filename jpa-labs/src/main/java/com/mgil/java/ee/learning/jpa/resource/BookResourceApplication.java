package com.mgil.java.ee.learning.jpa.resource;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/api")
public class BookResourceApplication extends Application {
}
