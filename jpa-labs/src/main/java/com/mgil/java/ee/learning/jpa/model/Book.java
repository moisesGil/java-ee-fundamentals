package com.mgil.java.ee.learning.jpa.model;


import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;


@Entity
@Table(name="books",schema = Book.SCHEMA)

@NamedQueries({
        @NamedQuery(name = Book.FIND_ALL_QUERY,query = "SELECT B FROM Book B ORDER BY B.id"),
        @NamedQuery(name=Book.COUNT_QUERY, query = "SELECT COUNT(B) FROM Book B")
})
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)

public class Book implements Serializable {

    public static final String FIND_ALL_QUERY = "Book.findAll";
    public static final String COUNT_QUERY = "Book.count";
    public static final String SCHEMA = "jpa_labs";

    @Id
    @SequenceGenerator(name="bookSequenceGenerator", schema = Book.SCHEMA,sequenceName = "books_seq",allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bookSequenceGenerator")
    private Long  id;
    private String title;
    private String description;
    private BigDecimal price;
    private String isbn;
    private Integer pages;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }
}
