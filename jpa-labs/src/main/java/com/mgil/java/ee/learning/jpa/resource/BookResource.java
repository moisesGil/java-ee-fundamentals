package com.mgil.java.ee.learning.jpa.resource;


import com.mgil.java.ee.learning.jpa.identifier.BookRepo;
import com.mgil.java.ee.learning.jpa.model.Book;
import com.mgil.java.ee.learning.jpa.model.Books;
import com.mgil.java.ee.learning.jpa.repositories.DataRepository;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.print.attribute.standard.Media;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("books")
@RequestScoped
public class BookResource {

    @Inject
    @BookRepo
    private DataRepository<Book,Long> bookRepository;

    @GET
    @Produces(MediaType.APPLICATION_XML)
    public Response getAllBooks() {

        List<Book> books = bookRepository.find();

        Response clientResponse;

        if (books.size() == 0)
            clientResponse = Response.noContent().build();
        else
            clientResponse = Response.ok(new Books(books)).build();

        return clientResponse;

    }


    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_XML)
    public Response createBook(Book book){


        if(book == null){
            return  Response.status(Response.Status.BAD_REQUEST).build();
        }

        book = bookRepository.save(book);

        System.out.println("Book created! ID: " + book.getId());

        return Response.status(Response.Status.CREATED).build();

    }


    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_XML)
    public Response findBook(@PathParam("id") Long id){

        Book book = bookRepository.find(id);

        if(book != null) {
            return Response.ok(book).build();
        }else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

    }

}
