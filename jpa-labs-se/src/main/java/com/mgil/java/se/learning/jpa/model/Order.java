package com.mgil.java.se.learning.jpa.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name="orders",schema = "jpa_labs")
public class Order {

    @Id
    @SequenceGenerator(name="orderGenerator", sequenceName = "order_seq",allocationSize = 1)
    @GeneratedValue(generator = "orderGenerator",strategy = GenerationType.SEQUENCE)
    private Long id;
    private String referenceId;
    private BigDecimal total;
    private BigDecimal subtotal;

    @ManyToOne
    private Customer customer;


    /**
     * By defaul in JPA a One-To-Many relationship is made with an intermediary table. With the annotation @JoinColumn this can be changed to a column.
     * In this case the relationship column will be in the OrderDetail object with the name "order_id".
     *
     * This will put the ID of the Order in the OrderDetail column.
     *
     */

    @OneToMany
    @JoinColumn(name="order_id")
    private List<OrderDetail> details;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<OrderDetail> getDetails() {
        return details;
    }

    public void setDetails(List<OrderDetail> details) {
        this.details = details;
    }
}
