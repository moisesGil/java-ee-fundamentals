package com.mgil.java.se.learning.jpa;

import com.mgil.java.se.learning.jpa.model.*;
import com.mgil.java.se.learning.jpa.repositories.BookRepository;
import com.mgil.java.se.learning.jpa.repositories.DataRepository;
import com.mgil.java.se.learning.jpa.repositories.PublisherRepository;
import com.mgil.java.se.learning.jpa.repositories.StoreRepository;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.mgil.java.se.learning.jpa.model.DbAction.COUNT;
import static com.mgil.java.se.learning.jpa.model.DbAction.CREATE;

public class Main {

    public static void main(String... args) {

        DbAction[] crudActions = {CREATE, COUNT};

        librarySetup();

    }


    private static void doBookOperations(DbAction... crudActions) {


        DataRepository<Book, Long> repository = new BookRepository();
        DataRepository<Publisher, Long> publisherRepository = new PublisherRepository();

        preparePublishers();

        Book book;
        Long bookId = 1000L;

        for (DbAction crudAction : crudActions) {

            switch (crudAction) {

                //Creating a new Entity

                case CREATE:


                    System.out.println("CREATE NEW BOOK");

                    Book newBook = new Book();

                    newBook.setTitle("Demo Book");
                    newBook.setDescription("Demo book created using JPA in Java SE");
                    newBook.setIsbn("13-12345-01");
                    newBook.setPrice(new BigDecimal(0.99));
                    newBook.setPages(123);


                    //Relationships

                    List<Publisher> publishers = publisherRepository.find();

                    if (publishers.size() > 0) {

                        newBook.setPublisher(publishers.get(0));

                    }

                    repository.save(newBook);

                    System.out.println("Book created!");

                    System.out.println(newBook);

                    break;


                case READ:

                    System.out.println("FIND A BOOK");

                    book = repository.find(bookId);
                    System.out.println(book);

                    break;


                case UPDATE:

                    System.out.println("UPDATE A BOOK");

                    book = repository.find(bookId);


                    System.out.println("Before to update:");
                    System.out.println(book);

                    System.out.println("Properties changed to update:");

                    book.setPages(200);
                    book.setPrice(new BigDecimal(2.99));

                    System.out.println("After update:");
                    book = repository.update(bookId, book);

                    System.out.println(book);


                    break;


                case COUNT:

                    System.out.println("BOOK COUNTING");

                    Long bookCount = repository.count();

                    System.out.println("Book counting: " + bookCount);

                    break;


            }

        }

        repository.close();


    }

    private static void doStoreOperations(DbAction... crudActions) {


        DataRepository<Store, Long> repository = new StoreRepository();

        Store store = null;

        for (DbAction crudAction : crudActions) {

            switch (crudAction) {

                //Creating a new Entity

                case CREATE:


                    System.out.println("CREATE NEW STORE");


                    store = new Store();

                    store.setTelephone("2122331234");
                    store.setFax("2121331230");
                    store.setAddress("Some address");
                    store.setEmail("dummy@store.com");

                    repository.save(store);

                    System.out.println("Store created!");

                    System.out.println(store);

                    break;


                case READ:

                    System.out.println("FIND A STORE");

                    store = repository.find(101L);
                    System.out.println(store);

                    break;


                case COUNT:

                    System.out.println("COUNT_QUERY STORES");

                    Long count = repository.count();

                    System.out.println("Store count = " + count);

                    break;

            }

        }

        repository.close();


    }

    private static void preparePublishers() {

        DataRepository<Publisher, Long> publisherRepository = new PublisherRepository();

        if (publisherRepository.count() > 0) {
            return;
        }

        System.out.print("Inserting publishers...");

        for (int x = 1; x < 4; x++) {

            Publisher publisher = new Publisher();

            publisher.setName("Demo Publisher " + x);
            publisher.setDescription("This is the demo publisher " + x);

            publisherRepository.save(publisher);

        }

        System.out.println("Done!");

    }

    private static void librarySetup() {

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("jpa-labs-se-pu");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        for (int x = 1; x < 3; x++) {

            List<Book> books = new ArrayList<Book>();

            for (int y = 1; y < 5; y++) {

                Book book = new Book();

                book.setTitle("Demo book " + y);
                book.setDescription("This is a demo book " + y);
                book.setPages(200 + y);
                book.setPrice(new BigDecimal(0.99 + y));
                book.setIsbn("13-1234-0" + y);
                book.setBookType(BookType.EBOOK);
                book.setRegistrationDate(new Date());

                books.add(book);

            }

            EntityTransaction tx = entityManager.getTransaction();


            System.out.println(tx);


            for (Book book : books) {

                tx.begin();

                entityManager.persist(book);

                tx.commit();

            }


            Library library = new Library();

            library.setName("Demo Library " + x);
            library.setBooks(books);

            tx = entityManager.getTransaction();

            System.out.println("*" + tx);

            tx.begin();

            entityManager.persist(library);

            tx.commit();

            TypedQuery<Library> findAllLibrariesQuery = entityManager.createQuery("SELECT L FROM Library L", Library.class);

            List<Library> libraries = findAllLibrariesQuery.getResultList();

            for (Library l : libraries) {

                System.out.println(l.getName());

                for (Book book : l.getBooks()) {
                    System.out.printf("\t-%s\n", book.getTitle());
                }

            }


        }


    }

}
