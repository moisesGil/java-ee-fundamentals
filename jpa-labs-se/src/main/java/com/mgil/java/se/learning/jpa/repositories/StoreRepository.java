package com.mgil.java.se.learning.jpa.repositories;

import com.mgil.java.se.learning.jpa.model.Store;

import javax.persistence.*;
import java.util.List;

public class StoreRepository implements DataRepository<Store, Long> {

    private EntityManager entityManager;

    public StoreRepository() {

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("jpa-labs-se-pu");
        entityManager = entityManagerFactory.createEntityManager();

    }

    @Override
    public Store find(Long entityId) {
        Store store = entityManager.find(Store.class, entityId);
        return store;
    }

    @Override
    public List<Store> find() {
        return null;
    }

    @Override
    public List<Store> find(int startIndex, int maxResult) {
        return null;
    }

    @Override
    public Store save(Store store) {

        EntityTransaction tx = entityManager.getTransaction();

        tx.begin();
        entityManager.persist(store);
        tx.commit();

        return store;
    }

    @Override
    public List<Store> save(List<Store> stores) {
        return null;
    }

    @Override
    public Long count()
    {

        TypedQuery<Long> countQuery = entityManager.createNamedQuery("Store.count",Long.class);

        Long count = countQuery.getSingleResult();

        return count ;
    }

    @Override
    public Store update(Long id, Store store) {
        return null;
    }

    @Override
    public void deleteById(Long entityId) {

    }

    @Override
    public void delete(Store store) {

    }

    @Override
    public void deleteIn(List<Store> stores) {

    }

    @Override
    public boolean idExists(Long entityId) {
        return false;
    }

    @Override
    public boolean entityExists(Store store) {
        return false;
    }

    @Override
    public void close() {

    }
}
