package com.mgil.java.se.learning.jpa.model;

import javax.persistence.*;
import java.math.BigDecimal;


@Entity
@Table(name="products",schema = "jpa_labs")
public class Product {

    @Id
    @SequenceGenerator(name="productGenerator",sequenceName = "products_seq", allocationSize = 1)
    @GeneratedValue(generator = "productGenerator",strategy = GenerationType.SEQUENCE)
    private Long id;
    private String name;
    private BigDecimal price;
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
