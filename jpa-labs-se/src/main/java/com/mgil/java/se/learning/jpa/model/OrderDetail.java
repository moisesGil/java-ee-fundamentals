package com.mgil.java.se.learning.jpa.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name="order_details",schema = "jpa_labs")
public class OrderDetail {

    @Id
    @SequenceGenerator(name="orderDetailGenerator",sequenceName = "order_details_seq", allocationSize = 1)
    @GeneratedValue(generator = "orderDetailGenerator",strategy = GenerationType.SEQUENCE)
    private Long id;

    @OneToOne
    private Product product;

    @Column(name="quantity",nullable = false)
    private Long quantity;

    @Column(name="detail_price",nullable = false)
    private BigDecimal detailPrice;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getDetailPrice() {
        return detailPrice;
    }

    public void setDetailPrice(BigDecimal detailPrice) {
        this.detailPrice = detailPrice;
    }
}
