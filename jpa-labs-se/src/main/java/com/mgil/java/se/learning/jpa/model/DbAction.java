package com.mgil.java.se.learning.jpa.model;

public enum DbAction {
    CREATE,READ,UPDATE,DELETE,COUNT
}
