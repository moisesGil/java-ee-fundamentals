package com.mgil.java.se.learning.jpa.model;


import javax.persistence.*;

@Entity
@Table(name="customers",schema = "jpa_labs")
public class Customer {

    @Id
    @SequenceGenerator(name="customerGenerator",allocationSize = 1,sequenceName = "customers_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customerGenerator")
    private Long id;

    @Column(nullable = false,length = 300)
    private String name;

    @Column(nullable = false, length = 500)
    private String lastName;

    @Column(name="document_id",  nullable = false)
    private String documentId;

    @Enumerated(EnumType.STRING)
    private DocumentIdType documentIdType;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }


    public DocumentIdType getDocumentIdType() {
        return documentIdType;
    }

    public void setDocumentIdType(DocumentIdType documentIdType) {
        this.documentIdType = documentIdType;
    }
}
