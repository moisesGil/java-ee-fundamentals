package com.mgil.java.se.learning.jpa.repositories;

import com.mgil.java.se.learning.jpa.model.Publisher;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

public class PublisherRepository implements DataRepository<Publisher, Long> {


    private EntityManager entityManager;


    public PublisherRepository() {

        entityManager = Persistence.createEntityManagerFactory("jpa-labs-se-pu").createEntityManager();

    }


    @Override
    public Publisher find(Long entityId) {
        Publisher publisher = entityManager.find(Publisher.class, entityId);
        return publisher;
    }

    @Override
    public List<Publisher> find() {
        TypedQuery<Publisher> findAllPublishersQuery = entityManager.createNamedQuery(Publisher.FIND_ALL_QUERY, Publisher.class);
        List<Publisher> publishers = findAllPublishersQuery.getResultList();
        return publishers;
    }

    @Override
    public List<Publisher> find(int startIndex, int maxResult) {
        return null;
    }

    @Override
    public Publisher save(Publisher publisher) {

        EntityTransaction tx = entityManager.getTransaction();

        tx.begin();

        entityManager.persist(publisher);

        tx.commit();

        return publisher;
    }

    @Override
    public List<Publisher> save(List<Publisher> publishers) {
        return null;
    }

    @Override
    public Long count() {

        TypedQuery<Long> publisherCountQuery = entityManager.createNamedQuery(Publisher.COUNT_QUERY, Long.class);
        Long publisherCount = publisherCountQuery.getSingleResult();

        return publisherCount;
    }

    @Override
    public Publisher update(Long id, Publisher publisher) {
        return null;
    }

    @Override
    public void deleteById(Long entityId) {

    }

    @Override
    public void delete(Publisher publisher) {

    }

    @Override
    public void deleteIn(List<Publisher> publishers) {

    }

    @Override
    public boolean idExists(Long entityId) {
        return false;
    }

    @Override
    public boolean entityExists(Publisher publisher) {
        return false;
    }

    @Override
    public void close() {
        entityManager.close();
    }
}
