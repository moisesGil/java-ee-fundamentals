package com.mgil.java.se.learning.jpa.model;


import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


@Entity

/**
 * Customizing the name of the mapping table, in this case this entity will map an entity called "books" in the database
 */
@Table(name="books",schema = Book.SCHEMA)

@NamedQueries({
        @NamedQuery(name = Book.FIND_ALL_QUERY,query = "SELECT B FROM Book B ORDER BY B.id"),
        @NamedQuery(name=Book.COUNT_QUERY, query = "SELECT COUNT(B) FROM Book B")
})
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Book implements Serializable {

    public static final String FIND_ALL_QUERY = "Book.findAll";
    public static final String COUNT_QUERY = "Book.count";
    public static final String SCHEMA = "jpa_labs";

    @Id
    @SequenceGenerator(name="bookSequenceGenerator", schema = Book.SCHEMA,sequenceName = "books_seq",allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bookSequenceGenerator")
    private Long  id;

    @Column(nullable = false,length = 300)
    private String title;

    @Column(nullable = false,length = 1000)
    private String description;

    @Column(nullable = false)
    private BigDecimal price;

    @Column(nullable = false)
    private String isbn;

    @Column(nullable = false)
    private Integer pages;

    /**
     * With the annotation "@temporal" it can be customize the way the date is saved in the database.
     */
    @Column(name = "registration_date")
    @Temporal(TemporalType.DATE)
    private Date registrationDate;

    /**
     * The column annotation allow to define several properties about the mapping column of the table.
     * The must common use of this annotation is for set the name of the column.
     */
    @Column(name="cover_image",length = 1500)
    private String coverImage;


    @Column(name="type")
    @Enumerated(EnumType.STRING)
    private BookType bookType;

    //Relationships
    @ManyToOne
    @JoinColumn(name="publisher")
    private Publisher publisher;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public BookType getBookType() {
        return bookType;
    }

    public void setBookType(BookType bookType) {
        this.bookType = bookType;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", isbn='" + isbn + '\'' +
                ", pages=" + pages +
                '}';
    }
}
