package com.mgil.java.se.learning.jpa.exes;

import com.mgil.java.se.learning.jpa.model.Product;
import com.mgil.java.se.learning.jpa.model.Store;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ManyToManyLabMain {

    public static void main(String args[]){


        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("jpa-labs-se-pu");
        EntityManager entityManager = entityManagerFactory.createEntityManager();



        System.out.print("Creating products...");

        for (int x = 0; x < 50; x++) {

            Product product = new Product();

            product.setName("Demo Product");
            product.setPrice(new BigDecimal(5.99));
            product.setDescription("This is a demo product");

            EntityTransaction tx = entityManager.getTransaction();

            tx.begin();

            entityManager.persist(product);

            tx.commit();

        }



        System.out.print("Retrieving products...");


        TypedQuery<Product> findAllProductsQuery = entityManager.createQuery("SELECT P FROM Product P",Product.class);

        List<Product> productList = findAllProductsQuery.getResultList();


        HashSet<Product> products = new HashSet<>();

        for(Product product : productList){
            products.add(product);
        }


        System.out.println("done!");




        System.out.print("Creating a Store...");


        EntityTransaction tx = entityManager.getTransaction();

        tx.begin();


        for(int x = 1 ; x < 3 ; x++){

            Store store = new Store();

            store.setTelephone("809-789-7854");
            store.setAddress("Some address...");
            store.setFax("809-555-7890");
            store.setEmail("some-bussiness@store.com");
            store.setProducts(products);

            entityManager.persist(store);

        }


        tx.commit();

        System.out.println("done!");


        entityManager.close();
        entityManagerFactory.close();


    }
}
