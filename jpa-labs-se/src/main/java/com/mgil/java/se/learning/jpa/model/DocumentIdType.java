package com.mgil.java.se.learning.jpa.model;

public enum DocumentIdType {
    DOCUMENT_ID,
    PASSPORT;
}
