package com.mgil.java.se.learning.jpa.model;

public enum BookType {

    EBOOK,PAPER,HARDCOVER;

}
