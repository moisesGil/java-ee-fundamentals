package com.mgil.java.se.learning.jpa.model;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="libraries")
public class Library {

    @Id
    @SequenceGenerator(name="libraryGenerator",sequenceName = "libraries_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "libraryGenerator")
    private Long id;

    private String name;

    @OneToMany
    @JoinColumn(name="library")
    private List<Book> books;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
