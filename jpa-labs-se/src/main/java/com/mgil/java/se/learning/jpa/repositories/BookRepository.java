package com.mgil.java.se.learning.jpa.repositories;

import com.mgil.java.se.learning.jpa.model.Book;

import javax.persistence.*;
import java.util.List;


/**
 * This class is a repository that use the EntityManager methods for
 * manage the operations with the database
 */

public class BookRepository implements DataRepository<Book, Long> {

    /**
     * Injecting the entity manager from the container with the annotatin "@PersistenceContext
     */

    private EntityManager entityManager;


    public BookRepository() {
        /**
         * Creating an entity
         *
         * For create an entity manager in SE, is required to first create a EntityManagerFactory.
         * The EntityManagerFactory requires the name of the persistence unit, that is declared in the persistence.xml file.
         *
         * Call the method createEntityManager for create an EntityManager
         *
         */
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-labs-se-pu");
        entityManager = emf.createEntityManager();

    }

    /**
     * Get a Book by ID if exists in the database
     *
     * @param bookId ID of the Book
     * @return a book, null if the ID does not exists in the database
     */
    @Override
    public Book find(Long bookId) {


        /**
         * Using the EntityManager method find that retrieve an entity from the database
         * if the ID passed exists. If the entity does not exist then the it returns null.
         *
         * The class type must be passed as argument to this method
         */

        Book book = entityManager.find(Book.class, bookId);

        return book;
    }

    /**
     * Retrieve all the books from the database
     *
     * @return All the books from the database
     */
    @Override
    public List<Book> find() {

        /**
         * Creating a named query for retrieve the books
         */
        TypedQuery<Book> findAllQuery = entityManager.createNamedQuery(Book.FIND_ALL_QUERY, Book.class);

        /**
         * Calling "getResultList" for obtain a collection with all the books in the database
         */
        List<Book> books = findAllQuery.getResultList();

        return books;
    }


    /**
     * This is a paginated find, that will use a index for start from that position and
     * max result for set the max quantity to retrieve
     *
     * @param startIndex Start index from where to start
     * @param maxResult  Maximum quantity of items to retrieve
     * @return A collection of books starting from the startIndex with the quantity defined in the maxResult
     */
    @Override
    public List<Book> find(int startIndex, int maxResult) {


        TypedQuery<Book> paginatedFindQuery = entityManager.createNamedQuery(Book.FIND_ALL_QUERY, Book.class);


        //Setting where to start
        paginatedFindQuery.setFirstResult(startIndex);

        //Setting the max quantity to retrieve
        paginatedFindQuery.setMaxResults(maxResult);

        List<Book> books = paginatedFindQuery.getResultList();

        return books;
    }


    /**
     * Method that persist an entity in the database
     *
     * @param book Entity to persist
     * @return An instance of the entity saved
     */
    @Override
    public Book save(Book book) {

        EntityTransaction tx = entityManager.getTransaction();

        tx.begin();

        entityManager.persist(book);

        tx.commit();
        return book;
    }

    /**
     * Method for save more than one entity
     *
     * @param books collection of the books to save
     * @return colleciton of books saved
     */
    @Override
    public List<Book> save(List<Book> books) {

        for (Book book : books) {

            entityManager.persist(book);

        }


        return books;
    }

    @Override
    public Long count() {

        Long books = entityManager.createNamedQuery(Book.COUNT_QUERY, Long.class).getSingleResult();

        return books;
    }

    @Override
    public Book update(Long id, Book book) {

        EntityTransaction tx = entityManager.getTransaction();

        Book savedBook = this.find(id);

        //TODO: Enhance this

        if (savedBook == null) {
            throw new IllegalArgumentException("Null entity");
        }

        tx.begin();

        savedBook.setTitle(book.getTitle());
        savedBook.setDescription(book.getDescription());
        savedBook.setPrice(book.getPrice());
        savedBook.setPages(book.getPages());
        savedBook.setIsbn(book.getIsbn());

        tx.commit();

        return savedBook;
    }


    /**
     * Remove a book
     *
     * @param entityId ID of the entity
     */
    @Override
    public void deleteById(Long entityId) {


        Book book = this.find(entityId);

        if (book != null) {

            //Removing a book using the entity manager method remove
            entityManager.remove(entityId);

        }


    }

    @Override
    public void delete(Book book) {

        EntityTransaction tx = entityManager.getTransaction();

        tx.begin();
        entityManager.remove(book);
        tx.commit();

    }

    @Override
    public void deleteIn(List<Book> books) {

        EntityTransaction tx = entityManager.getTransaction();

        tx.begin();

        for (Book book : books) {
            entityManager.remove(book);
        }

        tx.commit();

    }


    //TODO: Work in these methods
    @Override
    public boolean idExists(Long entityId) {


        return false;
    }

    @Override
    public boolean entityExists(Book book) {

        return false;

    }

    public void close(){
        this.entityManager.close();
    }


}
