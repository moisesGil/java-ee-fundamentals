package com.mgil.java.se.learning.jpa.repositories;

import java.io.Serializable;
import java.util.List;

public interface DataRepository<ENTITY extends Serializable,ID_TYPE extends Comparable> {


    //Find methods
    ENTITY find(ID_TYPE entityId);
    List<ENTITY> find();
    List<ENTITY> find(int startIndex, int maxResult);

    ENTITY save(ENTITY entity);
    List<ENTITY> save(List<ENTITY> entities);
    Long count();
    ENTITY update(ID_TYPE id, ENTITY entity);
    void deleteById(ID_TYPE entityId);
    void delete(ENTITY entity);
    void deleteIn(List<ENTITY> entities);
    boolean idExists(ID_TYPE entityId);
    boolean entityExists(ENTITY entity);
    void close();

}


