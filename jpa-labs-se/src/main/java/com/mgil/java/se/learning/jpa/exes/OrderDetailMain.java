package com.mgil.java.se.learning.jpa.exes;

import com.mgil.java.se.learning.jpa.model.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This code is an example of Order-OrderDetail relationship.
 */

public class OrderDetailMain {

    public static void main(String[] args) {

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("jpa-labs-se-pu");
        EntityManager entityManager = entityManagerFactory.createEntityManager();


        System.out.print("Creating products...");

        for (int x = 0; x < 50; x++) {

            Product product = new Product();

            product.setName("Demo Product");
            product.setPrice(new BigDecimal(5.99));
            product.setDescription("This is a demo product");

            EntityTransaction tx = entityManager.getTransaction();

            tx.begin();

            entityManager.persist(product);

            tx.commit();

        }

        System.out.println("done!");


        System.out.print("Creating customer...");


        Customer customer = new Customer();

        customer.setDocumentId("0014567891");
        customer.setDocumentIdType(DocumentIdType.DOCUMENT_ID);
        customer.setGender(Gender.MALE);
        customer.setName("Jhon");
        customer.setLastName("Appleseed");

        EntityTransaction tx = entityManager.getTransaction();

        tx.begin();

        entityManager.persist(customer);

        tx.commit();


        System.out.println("done!");


        System.out.println("Creating Order...");


        Order order = new Order();

        order.setCustomer(customer);
        order.setReferenceId("789789789");
        order.setSubtotal(BigDecimal.ZERO);
        order.setTotal(BigDecimal.ZERO);


        TypedQuery<Product> findAllProductQuery =  entityManager.createQuery("SELECT P FROM Product P",Product.class);

        List<Product> allProducts = findAllProductQuery.getResultList();

        Random random = new Random();


        List<OrderDetail> orderDetails  = new ArrayList<OrderDetail>();

        //Create details for these products

        for(int x = 1 ; x < 4 ; x++){


            OrderDetail orderDetail = new OrderDetail();

            orderDetail.setDetailPrice(new BigDecimal(9.5));
            orderDetail.setProduct(allProducts.get(random.nextInt(allProducts.size())));
            orderDetail.setQuantity(new Long(random.nextInt(10) + 1));
            orderDetail.setDetailPrice(new BigDecimal(orderDetail.getQuantity()).multiply(orderDetail.getProduct().getPrice()));

            orderDetails.add(orderDetail);

        }

        order.setDetails(orderDetails);


        //Saving details


        tx = entityManager.getTransaction();


        tx.begin();


        for(OrderDetail orderDetail : orderDetails){

            entityManager.persist(orderDetail);

        }

        tx.commit();


        System.out.print("Saving order...");

        tx = entityManager.getTransaction();

        tx.begin();

        entityManager.persist(order);

        tx.commit();

        System.out.println("done!");

    }
}
