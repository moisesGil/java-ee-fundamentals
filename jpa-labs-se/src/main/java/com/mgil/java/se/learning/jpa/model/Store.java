package com.mgil.java.se.learning.jpa.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * This entity is mapped using XML,look in the resource file
 */

//TODO: Research about why many to many relationship has to be in a set and it has to be on implentation and not in the interface
@Access(AccessType.FIELD)
@Entity
@Table(name="stores",schema="jpa_labs")
public class Store implements Serializable {

    @Id
    @SequenceGenerator(name = "storeGenerator", sequenceName = "stores_seq", allocationSize = 1)
    @GeneratedValue(generator = "storeGenerator", strategy = GenerationType.IDENTITY)
    private Long id;
    private String address;
    private String telephone;
    private String email;
    private String fax;

    @ManyToMany
    @JoinTable(schema = "jpa_labs")
    private Set<Product> products;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }
}
