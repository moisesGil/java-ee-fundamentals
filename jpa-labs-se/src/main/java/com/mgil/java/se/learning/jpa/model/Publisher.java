package com.mgil.java.se.learning.jpa.model;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="publishers")
@NamedQueries({
        @NamedQuery(name = Publisher.FIND_ALL_QUERY, query =  "SELECT P FROM Publisher P ORDER BY P.id"),
        @NamedQuery(name = Publisher.COUNT_QUERY, query =  "SELECT COUNT(P) FROM Publisher P"),
})
public class Publisher implements Serializable {

    public static final String FIND_ALL_QUERY = "Publisher.findAll";
    public static final String COUNT_QUERY = "Publisher.count";

    @Id
    @SequenceGenerator(name="publisherGenerator", sequenceName = "publishers_seq",allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "publisherGenerator")
    private Long id;

    @Column(name="name", nullable = false)
    private String name;

    private String description;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
