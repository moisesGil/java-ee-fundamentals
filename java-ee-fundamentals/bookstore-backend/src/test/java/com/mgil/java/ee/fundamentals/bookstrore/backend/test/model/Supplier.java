package com.mgil.java.ee.fundamentals.bookstrore.backend.test.model;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="suppliers")
public class Supplier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

}
