package com.mgil.java.ee.fundamentals.bookstrore.backend.test.model;

import lombok.Data;

import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
@Data
public class CD extends Item {

    private int minutesLength;
    private BigDecimal price;
    private String genre;

}
