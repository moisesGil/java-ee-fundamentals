package com.mgil.java.ee.fundamentals.bookstrore.backend.test.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="products")
public class Product extends Item{

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Manufacturer manufacturer;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Supplier supplier;

}
