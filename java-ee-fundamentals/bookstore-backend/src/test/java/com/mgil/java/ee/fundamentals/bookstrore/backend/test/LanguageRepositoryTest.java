package com.mgil.java.ee.fundamentals.bookstrore.backend.test;


import com.mgil.java.ee.fundamentals.bookstrore.backend.model.Book;
import com.mgil.java.ee.fundamentals.bookstrore.backend.model.Language;
import com.mgil.java.ee.fundamentals.bookstrore.backend.repository.BookRepository;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

@RunWith(Arquillian.class)
public class LanguageRepositoryTest {


    @Inject
    private BookRepository bookRepository;

    @Deployment
    public static Archive<?> doDeployment(){

        WebArchive archive = ShrinkWrap.create(WebArchive.class)
                .addPackages(true,"com.mgil.java.ee.fundamentals.bookstrore.backend")
                .addAsWebInfResource(EmptyAsset.INSTANCE,"beans.xml")
                .addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml");

        return archive;

    }


    @Test
    public void shouldRetrieveALanguage() {
        Book book = bookRepository.find(1L);
        Assert.assertNotNull(book);
    }
}
