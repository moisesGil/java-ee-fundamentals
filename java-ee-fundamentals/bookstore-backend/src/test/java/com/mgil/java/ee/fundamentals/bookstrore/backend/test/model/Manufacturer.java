package com.mgil.java.ee.fundamentals.bookstrore.backend.test.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="manufacturers")
public class Manufacturer {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
}
