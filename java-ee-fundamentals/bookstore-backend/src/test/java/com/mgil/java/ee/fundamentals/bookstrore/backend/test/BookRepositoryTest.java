package com.mgil.java.ee.fundamentals.bookstrore.backend.test;

import com.mgil.java.ee.fundamentals.bookstrore.backend.model.*;
import com.mgil.java.ee.fundamentals.bookstrore.backend.repository.BookRepository;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


@RunWith(Arquillian.class)
public class BookRepositoryTest {


    @Inject
    private BookRepository bookRepository;


    @Deployment
    public static Archive<?> packageAll() {

        WebArchive archive = ShrinkWrap.create(WebArchive.class)
                .addPackages(true,"com.mgil.java.ee.fundamentals.bookstrore.backend")
                .addAsWebInfResource(EmptyAsset.INSTANCE,"beans.xml")
                .addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml");


        return archive;
    }


    @Test
    public void shouldCreateBook() {

        Language language = new Language();
        language.setId(1L);

        Category category = new Category();
        category.setId(1L);

        Publisher publisher = new Publisher();
        publisher.setId(1L);

        Set<Author> authors = new HashSet<>();

        Author author = new Author();
        author.setId(1L);

        authors.add(author);


        Book book = null;

        bookRepository.create(book);

        Assert.assertEquals(Long.valueOf(1L),book.getId());

    }
}
