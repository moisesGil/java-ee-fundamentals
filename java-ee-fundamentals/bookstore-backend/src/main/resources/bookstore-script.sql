

--Authors
insert into authors(id,first_name,last_name,bio,birth_date) values (author_seq.nextval,'Juan','Marino Zapete','Here is the bio',DATE '1983-02-01');
insert into authors(id,first_name,last_name,bio,birth_date) values (author_seq.nextval,'Mary','Bush Sharp','Here is th',DATE '1973-09-21');

--Publishers
insert into publishers(id,name) values(publisher_seq.nextval,'Zafax Publications');
insert into publishers(id,name) values(publisher_seq.nextval,'Axxo Media,Inc.');

--Languages
insert into languages(id,name) values(language_seq.nextval,'Spanish');
insert into languages(id,name) values(language_seq.nextval,'English');
insert into languages(id,name) values(language_seq.nextval,'France');


--Categories
insert into categories(id,name) values(category_seq.nextval,'Science');
insert into categories(id,name) values(category_seq.nextval,'Non fiction');
insert into categories(id,name) values(category_seq.nextval,'Novel');
insert into categories(id,name) values(category_seq.nextval,'Idioms and Travel');

insert into books(id,title,isbn,pages,publication_date) values(book_seq.nextval,'Gramatica Espanñola','13-789-456',100,CURRENT_DATE);
