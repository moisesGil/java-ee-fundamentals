package com.mgil.java.ee.fundamentals.bookstrore.backend.resource;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


@ApplicationPath("/api")
public class MainResource extends Application {
}
