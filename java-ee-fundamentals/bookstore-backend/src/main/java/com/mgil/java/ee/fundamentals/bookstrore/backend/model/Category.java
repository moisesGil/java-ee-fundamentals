package com.mgil.java.ee.fundamentals.bookstrore.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="categories")
public class Category {

    @Id
    @SequenceGenerator(name = "categoryGenerator", sequenceName = "category_seq",allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "categoryGenerator")
    private Long id;

    @Column(name="name", nullable = false)
    private String name;

}
