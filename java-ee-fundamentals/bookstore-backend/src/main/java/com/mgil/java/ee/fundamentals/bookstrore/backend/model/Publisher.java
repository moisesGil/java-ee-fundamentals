package com.mgil.java.ee.fundamentals.bookstrore.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name="publishers")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Publisher {


    @Id
    @SequenceGenerator(name="publisherGenerator",sequenceName = "publisher_seq",allocationSize = 1)
    @GeneratedValue(generator = "publisherGenerator", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "name",nullable = false)
    private String name;

}
