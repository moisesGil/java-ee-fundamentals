package com.mgil.java.ee.fundamentals.bookstrore.backend.utils;

import com.mgil.java.ee.fundamentals.bookstrore.backend.model.PublicationIdentifierType;


public class IsbnGenerator implements PublicationIdentifierGenerator {


    private String idPreffix = "13-";
    private String idSuffix = "9";

    public PublicationIdentifierType getPublicationIdentifierType() {
        return PublicationIdentifierType.ISBN;
    }

    public String generatePublicationIdentifier() {
        String pubIdentifier = idPreffix + Math.random() + idSuffix;
        return pubIdentifier;
    }
}
