package com.mgil.java.ee.fundamentals.bookstrore.backend.repository;

import com.mgil.java.ee.fundamentals.bookstrore.backend.model.Language;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Singleton
public class LanguageRepository implements Repository<Language,Long> {

    @PersistenceContext(unitName = "bookStorePU")
    private EntityManager entityManager;

    @Override
    public Language create(Language language) {
        entityManager.persist(language);
        return language;
    }

    @Override
    public Language find(Long id) {
        Language language = entityManager.find(Language.class,id);
        return language;
    }

    @Override
    public List<Language> find() {
        return null;
    }

    @Override
    public List<Language> find(Integer startPosition, Integer maxResult) {
        return null;
    }

    @Override
    public Language update(Language entity) {
        return null;
    }

    @Override
    public Language deleteById(Long aLong) {
        return null;
    }

    @Override
    public Language delete(Language entity) {
        return null;
    }
}
