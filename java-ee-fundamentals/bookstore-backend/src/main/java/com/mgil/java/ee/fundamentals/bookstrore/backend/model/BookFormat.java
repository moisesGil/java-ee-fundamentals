package com.mgil.java.ee.fundamentals.bookstrore.backend.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="book_formats")
public class BookFormat {

    @Id
    @SequenceGenerator(name="bookFormatGenerator",sequenceName = "book_format_seq",allocationSize = 1)
    @GeneratedValue(generator = "bookFormatGenerator",strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name="name")
    private String name;

}
