package com.mgil.java.ee.fundamentals.bookstrore.backend.repository;

import javax.validation.constraints.NotNull;
import java.util.List;


//Added comment to this class for testing
public interface Repository<T,ID> {

    T create(@NotNull  T entity);
    T find(@NotNull ID id);
    List<T> find();
    List<T> find(Integer startPosition, Integer maxResult);
    T update(@NotNull T entity);
    T deleteById(@NotNull ID id);
    T delete (@NotNull T entity);

}
