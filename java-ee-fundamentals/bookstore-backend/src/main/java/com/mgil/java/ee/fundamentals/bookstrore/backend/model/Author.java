package com.mgil.java.ee.fundamentals.bookstrore.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="authors")
public class Author {

    @Id
    @SequenceGenerator(name="author_sequence",sequenceName = "author_seq",allocationSize = 1)
    @GeneratedValue(generator = "author_sequence",strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotNull
    @Column(name="first_name", nullable  = false, length = 55)
    private String firstName;

    @Column(name="last_name", nullable =  false, length =  55)
    private String lastName;

    @Column(name="bio", length = 500)
    private String bio;

    @Temporal(TemporalType.DATE)
    @Column(name="birth_date",nullable = false)
    private Date dateOfBirth;

    @Transient
    private Integer age;

}
