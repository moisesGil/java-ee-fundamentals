package com.mgil.java.ee.fundamentals.bookstrore.backend.model;

public enum PublicationIdentifierType {

    ISBN , ISSN
}
