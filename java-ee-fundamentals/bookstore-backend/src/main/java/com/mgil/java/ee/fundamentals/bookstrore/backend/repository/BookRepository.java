package com.mgil.java.ee.fundamentals.bookstrore.backend.repository;


import com.mgil.java.ee.fundamentals.bookstrore.backend.model.Book;
import com.mgil.java.ee.fundamentals.bookstrore.backend.model.BookQueries;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
@Transactional
public class BookRepository implements Repository<Book, Long> {

    @PersistenceContext(name = "bookStorePU")
    private EntityManager entityManager;


    public BookRepository() {
    }


    //For Test purposes
    public BookRepository(EntityManager entityManager) {

        this.entityManager = entityManager;

    }




    @Override
    public Book create(Book entity) {
        entityManager.persist(entity);
        return entity;
    }

    @Override
    public Book find(Long id) {
        Book book = entityManager.find(Book.class, id);
        return book;
    }

    //TODO: To implement this method later
    @Override
    public List<Book> find() {

        TypedQuery<Book> selectAllBooks = entityManager.createQuery(BookQueries.SELECT_ALL, Book.class);
        List<Book> result = selectAllBooks.getResultList();
        return result;
    }

    @Override
    public List<Book> find(Integer startPosition, Integer maxResult) {
        TypedQuery<Book> findAllQuery = entityManager
                .createQuery("SELECT DISTINCT b FROM Book b ORDER BY b.id", Book.class);

        findAllQuery.setFirstResult(startPosition);
        findAllQuery.setMaxResults(maxResult);

        List<Book> books = findAllQuery.getResultList();

        return books;
    }

    @Override
    public Book update(Book entity) {

        Book bookUpdated;

        if(!entityManager.contains(entity)){
             bookUpdated = entityManager.merge(entity);
        }else{
            bookUpdated = entity;
        }


        return bookUpdated;
    }

    @Override
    public Book deleteById(Long id) {

        Book book = this.find(id);

        if (book != null) {
            entityManager.remove(book);
        }

        return book;
    }

    @Override
    public Book delete(Book book) {
        entityManager.remove(book);
        return book;
    }
}
