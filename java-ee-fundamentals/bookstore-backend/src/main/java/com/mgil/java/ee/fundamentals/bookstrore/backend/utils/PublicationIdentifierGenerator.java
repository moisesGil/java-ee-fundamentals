package com.mgil.java.ee.fundamentals.bookstrore.backend.utils;

import com.mgil.java.ee.fundamentals.bookstrore.backend.model.PublicationIdentifierType;

public interface PublicationIdentifierGenerator {

    PublicationIdentifierType getPublicationIdentifierType();
    String generatePublicationIdentifier();

}
