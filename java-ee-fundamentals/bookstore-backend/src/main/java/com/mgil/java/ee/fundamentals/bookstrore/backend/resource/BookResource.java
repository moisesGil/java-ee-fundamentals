package com.mgil.java.ee.fundamentals.bookstrore.backend.resource;


import com.mgil.java.ee.fundamentals.bookstrore.backend.model.Book;
import com.mgil.java.ee.fundamentals.bookstrore.backend.repository.BookRepository;
import com.sun.deploy.net.HttpResponse;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.util.List;

@Path("books")
public class BookResource {

    @Inject
    private BookRepository bookRepository;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listBooks() {

        List<Book> books = bookRepository.find();

        Response response;

        if (books != null && books.size() > 0) {
            response = Response.ok(books).build();
        } else {
            response = Response.noContent().build();
        }

        return response;
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createBook(Book book) {
        Book newBook = bookRepository.create(book);
        return Response.status(Response.Status.CREATED).build();

    }

}
