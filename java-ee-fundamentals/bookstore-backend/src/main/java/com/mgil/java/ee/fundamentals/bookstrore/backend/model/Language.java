package com.mgil.java.ee.fundamentals.bookstrore.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name="languages")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Language {

    @Id
    @SequenceGenerator(name="languageGenerator",sequenceName = "language_seq",allocationSize = 1)
    @GeneratedValue(generator = "languageGenerator",strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "name",nullable = false)
    private String name;

}
