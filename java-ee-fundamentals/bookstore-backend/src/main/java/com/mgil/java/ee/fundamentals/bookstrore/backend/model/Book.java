package com.mgil.java.ee.fundamentals.bookstrore.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;
import java.util.Set;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="books")
public class Book {

    @Id
    @SequenceGenerator(name="bookSequence",sequenceName = "book_seq",allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "bookSequence")
    private Long id;

    @NotNull
    @Column(name="title", nullable = false)
    private String title;

    @Size(max=15)
    @Column(name="isbn",length = 15,nullable = false)
    private String isbn;

    @Min(10)
    @Column(name="pages",nullable = false)
    private Integer pages;

    @Temporal(TemporalType.DATE)
    @Column(name="publication_date",nullable=false)
    private Date publicationDate;

    //Relationships

    @ManyToMany
    @JoinTable(name = "books_authors")
    private Set<Author> authors;

    @ManyToOne
    private Language language;

    @ManyToOne
    private Publisher publisher;

    @ManyToMany(cascade = CascadeType.PERSIST)
    private Set<Category> category;

    @OneToMany
    private List<BookFormat> format;

}
