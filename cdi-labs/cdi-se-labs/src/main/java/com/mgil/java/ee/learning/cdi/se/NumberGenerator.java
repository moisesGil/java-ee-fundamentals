package com.mgil.java.ee.learning.cdi.se;

public interface NumberGenerator {
    String generateNumber();
}
